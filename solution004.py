class Solution(object):
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        m = len(nums1)
        n = len(nums2)
        m_end = m == 0
        n_end = n == 0
        if m_end and n_end:
            return 0.0  # both are empty
        target = (m + n) / 2
        i = 0
        mi = 0
        ni = 0
        current = 0
        while i < target:
            if m_end:
                current = nums2[ni]
                ni += 1
                n_end = ni == n
            elif n_end:
                current = nums1[mi]
                mi += 1
                m_end = mi == m
            else:
                if nums1[mi] <= nums2[ni]:
                    current = nums1[mi]
                    mi += 1
                    m_end = mi == m
                else:
                    current = nums2[ni]
                    ni += 1
                    n_end = ni == n
            i += 1
        median = nums2[ni] if m_end else (nums1[mi] if n_end else min([nums1[mi], nums2[ni]]))
        if (m + n) % 2 == 0:
            return (current + median) / float(2)
        else:
            return median


if __name__ == '__main__':
    print Solution().findMedianSortedArrays([1, 2], [3, 4])
    print Solution().findMedianSortedArrays([1, 2], [3, 4, 5])
    print Solution().findMedianSortedArrays([1, 2], [3, 4, 5, 6])
    print Solution().findMedianSortedArrays([1, 2, 3], [3, 4, 5, 6])
    print Solution().findMedianSortedArrays([1, 2, 5], [3, 4, 5, 6])
    print Solution().findMedianSortedArrays([], [1])
    print Solution().findMedianSortedArrays([], [])
