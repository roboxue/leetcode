class LRUCache(object):
    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.capacity = capacity
        self.usage = []
        self.memory = {}
        self.head = None
        self.tail = None

    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        if key not in self.memory:
            return -1
        else:
            found = self.memory[key]
            to_return = found.value
            self.used(found)
            return to_return

    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: void
        """
        if self.capacity == 0:
            return
        if key not in self.memory:
            node = Node(key, value, None, None)
            if len(self.memory) == self.capacity:
                key_to_del = self.head.key
                if self.capacity == 1:
                    self.head = None
                    self.tail = None
                else:
                    self.head.next_node.previous_node = None
                    self.head = self.head.next_node
                del self.memory[key_to_del]

            if len(self.memory) == 0:
                self.head = node
                self.tail = node
            else:
                self.tail.next_node = node
                node.previous_node = self.tail
                self.tail = node
            self.memory[key] = node
        else:
            self.memory[key].value = value
            self.used(self.memory[key])

    def used(self, node):
        if len(self.memory) > 1 and node is not self.tail:
            if node is not self.head:
                node.previous_node.next_node = node.next_node
                node.next_node.previous_node = node.previous_node
            else:
                self.head = self.head.next_node
                self.head.previous_node = None
            node.previous_node = self.tail
            node.next_node = None
            self.tail.next_node = node
            self.tail = node


class Node(object):
    def __init__(self, key, value, previous_node, next_node):
        self.key = key
        self.value = value
        self.previous_node = previous_node
        self.next_node = next_node

        # Your LRUCache object will be instantiated and called as such:
        # obj = LRUCache(capacity)
        # param_1 = obj.get(key)
        # obj.put(key,value)

if __name__ == '__main__':
    cache = LRUCache(2)
    cache.put(1, 1)
    cache.put(2, 2)
    print cache.get(1)
    cache.put(3, 3)
    print cache.get(2)
    cache.put(4, 4)
    print cache.get(1)
    print cache.get(3)
    print cache.get(4)

    cache = LRUCache(0)
    cache.put(1, 1)
    cache.put(2, 2)
    print cache.get(1)
    cache.put(3, 3)
    print cache.get(2)
    cache.put(4, 4)
    print cache.get(1)
    print cache.get(3)
    print cache.get(4)

    cache = LRUCache(1)
    cache.put(1, 1)
    cache.put(2, 2)
    print cache.get(1)
    cache.put(3, 3)
    print cache.get(2)
    cache.put(4, 4)
    print cache.get(1)
    print cache.get(3)
    print cache.get(4)
