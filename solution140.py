class Solution(object):
    def __init__(self):
        self.cache = {}

    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: List[str]
        """
        if s not in self.cache:
            result = []
            for word in wordDict:
                w_len = len(word)
                if s == word:
                    result.append(word)
                elif s[:w_len] == word:
                    result.extend(map(lambda x: word + ' ' + x, self.wordBreak(s[w_len:], wordDict)))
            self.cache[s] = result
        return self.cache[s]
