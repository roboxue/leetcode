class Solution(object):
    def flipLights(self, n, m):
        """
        :type n: int
        :type m: int
        :rtype: int
        """
        s = set()
        for p in self.patterns(m):
            s.add(self.pattern(n, p))
        print self.patterns(m)
        print s
        return len(s)


    def patterns(self, m):
        if m == 0:
            return [[0, 0, 0, 0]]
        if m % 4 == 0:
            return [[0, 0, 0, 0], [1, 1, 1, 1]]
        if m % 4 == 1:
            return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]
        if m % 4 == 2:
            return [[1, 1, 0, 0], [1, 0, 1, 0], [1, 0, 0, 1], [0, 1, 1, 0], [0, 1, 0, 1], [0, 0, 1, 1]]
        if m % 4 == 3:
            return [[1, 1, 1, 0], [1, 1, 0, 1], [1, 0, 1, 1], [0, 1, 1, 1], [1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

    def pattern(self, n, p):
        state = 0b111111
        if p[0] == 1:
            state ^= 0b111111
        if p[1] == 1:
            state ^= 0b010101
        if p[2] == 1:
            state ^= 0b101010
        if p[3] == 1:
            state ^= 0b100100
        shift = 0 if n >= 6 else (6 - (n % 6))
        return state >> shift


if __name__ == '__main__':
    print Solution().flipLights(1, 1)
    print Solution().flipLights(2, 1)
    print Solution().flipLights(3, 1)
    print Solution().flipLights(6, 1)
