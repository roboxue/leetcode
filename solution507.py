import math


class Solution(object):
    def checkPerfectNumber(self, num):
        """
        :type num: int
        :rtype: bool
        """
        if num <= 0:
            return False
        sum = 1
        for i in range(2, int(math.sqrt(num)) + 1):
            if num % i == 0:
                sum += i + num / i
        return sum == num

if __name__ == '__main__':
    print Solution().checkPerfectNumber(28)
    print Solution().checkPerfectNumber(27)
