class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        flag = 1
        output = []
        for d in reversed(digits):
            temp = flag + d
            if temp == 10:
                flag = 1
                output.append(0)
            else:
                flag = 0
                output.append(temp)
        if flag == 1:
            output.append(1)
        return list(reversed(output))


if __name__ == '__main__':
    s = Solution()
    print s.plusOne([9, 0, 1])
    print s.plusOne([0])
    print s.plusOne([1])
    print s.plusOne([9])
    print s.plusOne([9, 1])
