from itertools import repeat
class Solution(object):
    def productExceptSelf(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        temp1 = 1
        temp2 = 1
        length = len(nums)
        result = list(repeat(1, length))
        for i, n in enumerate(nums):
            j = length - i - 1
            result[i] *= temp1
            result[j] *= temp2
            temp1 *= nums[i]
            temp2 *= nums[j]
        return result
