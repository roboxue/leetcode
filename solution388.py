class Solution(object):
    def lengthLongestPath(self, input):
        """
        :type input: str
        :rtype: int
        """
        dirs = ['']
        dirsLen = 0
        maxLen = 0
        for l in input.split('\n'):
            clean_l = l.replace('\t', '').replace('    ', '')
            if '.' in l:
                length = dirsLen + len(clean_l)
                if length > maxLen:
                    maxLen = length
            else:
                tabs = l.count('\t') + l.count('    ') + 1
                if tabs > len(dirs):
                    dirs.append(clean_l)
                elif tabs < len(dirs):
                    dirs = dirs[:tabs]
                else:
                    dirs.pop()
                    dirs.append(clean_l)
                dirsLen = len('.'.join(dirs)) + 1
        return maxLen


if __name__ == '__main__':
    print Solution().lengthLongestPath("dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext")
    print Solution().lengthLongestPath("file3.txt\ndir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext")

