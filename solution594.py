class Solution(object):
    def findLHS(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        temp = {}
        for i, n in enumerate(nums):
            if n in temp:
                temp[n].append(n)
            else:
                temp[n] = [n]
            if n - 1 in temp:
                temp[n - 1].append(n)
            else:
                temp[n - 1] = [n]
        eligible = filter(lambda x: len(set(x)) == 2, temp.values())
        if len(eligible) == 0:
            return 0
        else:
            return max(map(lambda x: len(x), eligible))


if __name__ == '__main__':
    print Solution().findLHS([1, 3, 2, 2, 5, 2, 3, 7])
