class Solution(object):
    def trap(self, height):
        """
        :type height: List[int]
        :rtype: int
        """

        layers = []
        total = 0
        last = 0
        for i, s in enumerate(height):
            if s > last:
                openLayers = layers
                remainingLayers = []
                for l in openLayers:
                    layerIndex, layerStart, layerEnd = l
                    if min(s, layerEnd) > max(last, layerStart):
                        total += (min(s, layerEnd) - max(last, layerStart)) * (i - layerIndex)
                        if s < layerEnd:
                            remainingLayers.append((layerIndex, s, layerEnd))
                    else:
                        remainingLayers.append(l)
                layers = remainingLayers
            elif s < last:
                layers.append((i, s, last))
            last = s
        return total


if __name__ == '__main__':
    print Solution().trap([0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1])
    print Solution().trap([5, 4, 1, 2])
