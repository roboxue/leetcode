# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def mergeKLists(self, inLists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """
        lists = filter(lambda l: l is not None, inLists)
        if len(lists) == 0:
            return None

        def minList(lists):
            minV = None
            minI = None
            for i, l in enumerate(lists):
                if minV is None or l.val < minV.val:
                    minV = l
                    minI = i
            return minV, minI

        head, i = minList(lists)
        lists.pop(i)
        current = head

        while len(lists) > 0:
            nextMin, nextMinI = minList(lists)
            while current.next is not None and current.next.val <= nextMin.val:
                current = current.next
            if current.next is not None:
                lists.append(current.next)
            current.next = nextMin
            lists.pop(nextMinI)
            current = nextMin

        return head


if __name__ == '__main__':
    n1 = ListNode(-1)
    n1.next = ListNode(1)
    n2 = ListNode(-3)
    n2.next = ListNode(1)
    n2.next.next = ListNode(4)
    n3 = ListNode(-2)
    n3.next = ListNode(-1)
    n3.next.next = ListNode(0)
    n3.next.next.next = ListNode(2)

    root = Solution().mergeKLists([n1, n2, n3])
    while root:
        print root.val
        root = root.next
