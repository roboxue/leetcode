class Solution(object):
    def shortestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        if len(s) == 0:
            return ""
        for i in range((len(s) + 1) / 2 - 1, -1, -1):
            if self.isPalindrome(s, i):
                return s[i * 2 + 1:][::-1] + s
            elif self.isPalindrome2(s, i):
                return s[i * 2::][::-1] + s

    def isPalindrome(self, s, index):
        result = True
        t = 0
        while result and t < index:
            if s[index - t - 1] == s[index + t + 1]:
                t += 1
            else:
                result = False
        return result

    def isPalindrome2(self, s, index):
        result = True
        t = 0
        while result and t < index:
            if s[index - t - 1] == s[index + t]:
                t += 1
            else:
                result = False
        return result


if __name__ == '__main__':
    print Solution().shortestPalindrome("abcde")
    print Solution().shortestPalindrome("baabcde")
    print Solution().shortestPalindrome("abbacd")

#TLE
