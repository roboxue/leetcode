class MinStack(object):
    def __init__(self):
        """
        initialize your data structure here.
        """
        self.min_value = []
        self.memory = []

    def push(self, x):
        """
        :type x: int
        :rtype: void
        """
        self.memory.append(x)
        if len(self.min_value) == 0 or self.min_value[-1] >= x:
            self.min_value.append(x)

    def pop(self):
        """
        :rtype: void
        """
        poped = self.memory[-1]
        self.memory = self.memory[:-1]
        if self.getMin() == poped:
            self.min_value = self.min_value[:-1]

    def top(self):
        """
        :rtype: int
        """
        return self.memory[-1]

    def getMin(self):
        """
        :rtype: int
        """
        return self.min_value[-1]



        # Your MinStack object will be instantiated and called as such:
        # obj = MinStack()
        # obj.push(x)
        # obj.pop()
        # param_3 = obj.top()
        # param_4 = obj.getMin()


if __name__ == '__main__':
    stack = MinStack()
    stack.push(-2)
    stack.push(0)
    stack.push(-3)
    print stack.getMin()
    stack.pop()
    print stack.top()
    print stack.getMin()
