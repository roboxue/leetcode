object Solution {
  def twoSum(nums: Array[Int], target: Int): Array[Int] = {
    val cache = scala.collection.mutable.Map.empty[Int, Int]
    for ((n, i) <- nums.zipWithIndex) {
      if (cache.contains(n)) {
        return Array(cache(n), i)
      } else {
        cache(target - n) = i
      }
    }
  }
}
