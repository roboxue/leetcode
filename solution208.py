class Trie(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.memory = {}

    def insert(self, word):
        """
        Inserts a word into the trie.
        :type word: str
        :rtype: void
        """
        lookup = self.memory
        for c in word + '#':
            if c not in lookup:
                lookup[c] = {}
            lookup = lookup[c]

    def search(self, word):
        """
        Returns if the word is in the trie.
        :type word: str
        :rtype: bool
        """
        lookup = self.memory
        for c in word:
            if c in lookup:
                lookup = lookup[c]
            else:
                return False
        return '#' in lookup

    def startsWith(self, prefix):
        """
        Returns if there is any word in the trie that starts with the given prefix.
        :type prefix: str
        :rtype: bool
        """
        lookup = self.memory
        for c in prefix:
            if c in lookup:
                lookup = lookup[c]
            else:
                return False
        return True


        # Your Trie object will be instantiated and called as such:
        # obj = Trie()
        # obj.insert(word)
        # param_2 = obj.search(word)
        # param_3 = obj.startsWith(prefix)

if __name__ == '__main__':
    obj = Trie()
    obj.insert("foo")
    obj.insert("bar")
    obj.insert("barfoo")
    obj.insert("b")

    print obj.search("f")
    print obj.search("foo")
    print obj.search("ba")
    print obj.search("bar")

    print obj.startsWith("f")
    print obj.startsWith("foo")
    print obj.startsWith("b")
    print obj.startsWith("ba")
    print obj.startsWith("bar")
    print obj.startsWith("barc")
    print obj.startsWith("barf")
    print obj.startsWith("z")
