class Solution(object):
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        mapping = {'1': ['*'],
                   '2': ['a', 'b', 'c'],
                   '3': ['d', 'e', 'f'],
                   '4': ['g', 'h', 'i'],
                   '5': ['j', 'k', 'l'],
                   '6': ['m', 'n', 'o'],
                   '7': ['p', 'q', 'r', 's'],
                   '8': ['t', 'u', 'v'],
                   '9': ['w', 'x', 'y', 'z']
                   }
        results = []
        for digit in digits:
            if len(results) == 0:
                results = mapping[digit]
            else:
                results = self.extender(mapping[digit], results)
        return results

    def extender(self, possible_digits, existing_results):
        r = []
        for c in possible_digits:
            for result in existing_results:
                r.append(result + c)
        return r

if __name__ == '__main__':
    print Solution().letterCombinations('213')
