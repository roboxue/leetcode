from treelib import stringToTreeNode, treeNodeToString, TreeNode

class Solution(object):
    def trimBST(self, root, L, R):
        """
        :type root: TreeNode
        :type L: int
        :type R: int
        :rtype: TreeNode
        """
        return self.trimRight(self.trimLeft(root, L), R)

    def trimLeft(self, root, L):
        if root is None:
            return None
        elif root.val == L:
            root.left = None
            return root
        elif root.val < L:
            return self.trimLeft(root.right, L)
        else:
            root.left = self.trimLeft(root.left, L)
            return root

    def trimRight(self, root, R):
        if root is None:
            return None
        elif root.val == R:
            root.right = None
            return root
        elif root.val < R:
            root.right = self.trimRight(root.right, R)
            return root
        else:
            return self.trimRight(root.left, R)

if __name__ == '__main__':
    root = stringToTreeNode("[3,1,4,null,2]")
    print treeNodeToString(root)
    print treeNodeToString(Solution().trimBST(root, 1, 2))