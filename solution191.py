import math

class Solution(object):
    def hammingWeight(self, n):
        """
        :type n: int
        :rtype: int
        """
        cap = math.pow(2, int(math.log(n, 2)))
        ones = 0
        while cap >= 1:
            if n >= cap:
                ones += 1
                n -= cap
            cap /= 2
        return ones

if __name__ == '__main__':
    print Solution().hammingWeight(11)