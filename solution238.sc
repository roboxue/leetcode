object Solution {
  def productExceptSelf(nums: Array[Int]): Array[Int] = {
    var temp1 = 1
    var temp2 = 1
    val len = nums.length
    val result = Array.fill(len)(1)
    nums.indices.map(i => {
      val j = len - i - 1
      result(i) *= temp1
      result(j) *= temp2
      temp1 *= nums(i)
      temp2 *= nums(j)
    }).toArray
  }
}
