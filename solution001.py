class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        demand = {}
        for i, n in enumerate(nums):
            if n in demand:
                return [demand[n], i]
            else:
                demand[target - n] = i
