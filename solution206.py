# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if head is None:
            return None
        last = None
        reverseHead = head
        current = head
        while True:
            reverseHead = ListNode(current.val)
            reverseHead.next = last
            last = reverseHead
            if current.next:
                current = current.next
            else:
                return reverseHead



