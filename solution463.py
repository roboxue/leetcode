class Solution(object):
    def islandPerimeter(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        sum = 0
        height = len(grid)
        width = len(grid[0])
        for i in range(height):
            for j in range(width):
                cell = grid[i][j]
                if cell == 1:
                    sum += 4
                    sum -= 0 if j == 0 else grid[i][j - 1]#left
                    sum -= 0 if i == 0 else grid[i - 1][j]#up
                    sum -= 0 if j == width - 1 else grid[i][j + 1]#right
                    sum -= 0 if i == height - 1 else grid[i + 1][j]#down
        return sum

if __name__ == '__main__':
    print Solution().islandPerimeter([[0,1,0,0],
        [1,1,1,0],
        [0,1,0,0],
        [1,1,0,0]])

