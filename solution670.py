class Solution(object):
    def maximumSwap(self, num):
        """
        :type num: int
        :rtype: int
        """
        return int(self.maxSwapChar(list(str(num))))


    def maxSwapChar(self, digits):
        sortedDigits = sorted(digits, reverse=True)
        result = digits[:]
        for i, d in enumerate(digits):
            if d == sortedDigits[i]:
                continue
            else:
                j = len(digits) - 1 - digits[::-1].index(sortedDigits[i])
                result[i] = sortedDigits[i]
                result[j] = d
                break
        return ''.join(result)

if __name__ == '__main__':
    print Solution().maximumSwap(9973)
    print Solution().maximumSwap(2736)
    print Solution().maximumSwap(12335431)
